package com.example.notes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private List<ModelClass> userList;
    private OnNoteListener mOnNoteListener;

    public  Adapter (List<ModelClass> userList, OnNoteListener mOnNoteListener){
        this.userList= userList;
        this.mOnNoteListener = mOnNoteListener;
    }

    @NonNull
    @Override
    public Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.design, parent, false);
        return new ViewHolder(view, mOnNoteListener);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter.ViewHolder holder, int position) {
        String note = userList.get(position).getBody();
        String line = userList.get(position).getDivider();
        holder.setData(note, line);


    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView body;
        private TextView divider;
        OnNoteListener onNoteListener;

        public ViewHolder(@NonNull View itemView, OnNoteListener onNoteListener) {
            super(itemView);
            body= itemView.findViewById(R.id.noteText);
            divider = itemView.findViewById(R.id.divider);
            this.onNoteListener = onNoteListener;
            itemView.setOnClickListener(this);

        }

        public void setData(String note, String line) {
            body.setText(note);
            divider.setText(line);
        }

        @Override
        public void onClick(View view) {
            onNoteListener.onNoteCLick(getAdapterPosition());
        }
    }

    public interface OnNoteListener{
        void onNoteCLick(int position);

    }
}
