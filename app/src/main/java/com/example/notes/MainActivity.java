package com.example.notes;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.HashSet;

public class MainActivity extends AppCompatActivity {

    int RC_SIGN_IN = 0;
    GoogleSignInClient mGoogleSignInClient;
    SignInButton button;
    String personName ;
    String personEmail ;
    public ArrayList<String> noteList = new ArrayList<>();
    SharedPreferences sharedPreferences;
   HashSet<String> hashSet;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if (personName!=null){
            if (item.getItemId() == R.id.newNote) {
                replaceFragment(new CreateNote());
                return true;
            }
            return true;
        }else{
            Toast.makeText(this, "Please Sign-in First", Toast.LENGTH_SHORT).show();
            return true;

        }
    }
    private void signIn() {
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void replaceFragment(Fragment fragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameFrag,fragment);
        fragmentTransaction.commit();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getApplicationContext().getSharedPreferences("NoteData", Context.MODE_PRIVATE);
        HashSet<String> newSet = (HashSet<String>) sharedPreferences.getStringSet("data", null);
        if (newSet==null){
            noteList.add("Welcome to the App!!");

        }else{
            noteList = new ArrayList<>(newSet);
        }
        button = findViewById(R.id.sign_in_button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                       switch (v.getId()) {
                           case R.id.sign_in_button:
                               signIn();
                               break;
                           // ...
                       }


                }
            });
            hashSet = new HashSet<>(noteList);
        sharedPreferences = this.getSharedPreferences("NotesList", MODE_PRIVATE);
        sharedPreferences.edit().putStringSet("nls", hashSet).apply();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            replaceFragment(new DisplayArea());
            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
            if (acct != null) {
                personName = acct.getDisplayName();
                personEmail = acct.getEmail();
                Toast.makeText(this, "Welcome "+personName +"!!", Toast.LENGTH_SHORT).show();

                SharedPreferences pref = getSharedPreferences("userInfo",Context.MODE_PRIVATE);
                SharedPreferences.Editor edt = pref.edit();
                edt.putString("user_name", personName);
                edt.putString("email_id", personEmail);
                edt.apply();
                button.setAlpha(0);
                button.setActivated(false);
            }
        } catch (ApiException e) {
            Log.w("error", "signInResult:failed code=" + e.getStatusCode());
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
    }



}