package com.example.notes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * create an instance of this fragment.
 */
public class CreateNote extends Fragment {
    View view;
    Button finish;
    EditText  body;
    MainActivity mainActivity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_create_note, container, false);
        body = view.findViewById(R.id.body);
        mainActivity = (MainActivity) getActivity();

        finish = view.findViewById(R.id.finish);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fbody =  body.getText().toString();
                mainActivity.noteList.add(fbody);
                HashSet<String> hashSet = new HashSet<>(mainActivity.noteList);
                SharedPreferences pref = Objects.requireNonNull(getActivity()).getSharedPreferences("NoteData",Context.MODE_PRIVATE);
                SharedPreferences.Editor edt = pref.edit();
                edt.putStringSet("data", hashSet);
                edt.apply();
                Bundle bundle = new Bundle();
                bundle.putString("key", fbody);
                DisplayArea displayArea = new DisplayArea();
                displayArea.setArguments(bundle);
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction().replace(R.id.frameFrag, displayArea).commit();

            }
        });
        return view;
    }


}