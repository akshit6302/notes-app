package com.example.notes;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.HashSet;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class Edit extends Fragment {
    View view;
    int tokenId;
    Button delete, update;
    EditText editText;
    MainActivity mainActivity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_edit, container, false);
        delete = view.findViewById(R.id.delete);
        update = view.findViewById(R.id.update);
        editText = view.findViewById(R.id.bodyEdit);
        mainActivity = (MainActivity) getActivity();
        Bundle bundle = getArguments();
        if (bundle!= null){
            tokenId = bundle.getInt("noteId");
        }
        //Log.i("editId", Integer.toString(tokenId));
        assert mainActivity != null;
        editText.setText(mainActivity.noteList.get(tokenId));
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(Objects.requireNonNull(getActivity())).setIcon(android.R.drawable.ic_delete)
                        .setTitle("Delete Note!")
                        .setMessage("Do you want to delete this note?")
                        .setPositiveButton("Yes", new Dialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mainActivity.noteList.remove(tokenId);
                                DisplayArea displayArea = new DisplayArea();
                                //displayArea.setArguments(bundle);
                                assert getFragmentManager() != null;
                                getFragmentManager().beginTransaction().replace(R.id.frameFrag, displayArea).commit();
                                HashSet<String> hashSet = new HashSet<>(mainActivity.noteList);
                                SharedPreferences pref = Objects.requireNonNull(getActivity()).getSharedPreferences("NoteData", Context.MODE_PRIVATE);
                                SharedPreferences.Editor edt = pref.edit();
                                edt.putStringSet("data", hashSet);
                                edt.apply();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();


            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String temp = editText.getText().toString();
                mainActivity.noteList.add(tokenId, temp);
                DisplayArea displayArea = new DisplayArea();
                //displayArea.setArguments(bundle);
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction().replace(R.id.frameFrag, displayArea).commit();
                HashSet<String> hashSet = new HashSet<>(mainActivity.noteList);
                SharedPreferences pref = Objects.requireNonNull(getActivity()).getSharedPreferences("NoteData", Context.MODE_PRIVATE);
                SharedPreferences.Editor edt = pref.edit();
                edt.putStringSet("data", hashSet);
                edt.apply();
            }
        });
        return view;
    }
}