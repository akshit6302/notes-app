package com.example.notes;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DisplayArea extends Fragment implements Adapter.OnNoteListener {
    View view;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    List<ModelClass> userList;
    Adapter adapter;
    ArrayList<String>list1 = new ArrayList<>();
    MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_display_area, container, false);
        mainActivity = (MainActivity) getActivity();
        initData();
        initRecylerView();
        return view;

    }


    private void initRecylerView() {
        recyclerView = view.findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter= new Adapter(userList, this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }

    private void initData() {
        userList = new ArrayList<>();
        for (int i =0; i<mainActivity.noteList.size(); i++){
          userList.add(new ModelClass(mainActivity.noteList.get(i),"----------------------------------"));
        }
    }

    @Override
    public void onNoteCLick(int position) {
            Log.i("tagId", Integer.toString(position));
            Bundle bundle = new Bundle();
            bundle.putInt("noteId", position);
            Edit edit = new Edit();
            edit.setArguments(bundle);
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().replace(R.id.frameFrag, edit).addToBackStack(null).commit();
    }
}