package com.example.notes;

public class ModelClass {
    private String body;
    private String divider;

    public ModelClass(String body, String divider) {
        this.body = body;
        this.divider = divider;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDivider() {
        return divider;
    }

    public void setDivider(String divider) {
        this.divider = divider;
    }

}
